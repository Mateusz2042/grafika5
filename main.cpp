#include <GL\freeglut.h>
#include <iostream>
#include "Vectors3D.h"
#include "materials.h"

enum
{
	BRASS,                // mosi�dz
	BRONZE,               // br�z
	POLISHED_BRONZE,      // polerowany br�z
	CHROME,               // chrom
	COPPER,               // mied�
	POLISHED_COPPER,      // polerowana mied�
	GOLD,                 // z�oto
	POLISHED_GOLD,        // polerowane z�oto
	PEWTER,               // grafit (cyna z o�owiem)
	SILVER,               // srebro
	POLISHED_SILVER,      // polerowane srebro
	EMERALD,              // szmaragd
	JADE,                 // jadeit
	OBSIDIAN,             // obsydian
	PEARL,                // per�a
	RUBY,                 // rubin
	TURQUOISE,            // turkus
	BLACK_PLASTIC,        // czarny plastik
	BLACK_RUBBER,         // czarna guma
};

int button_x;
int button_y;
int button_state = GLUT_UP;
float rotatex;
float rotatey;

float lightPosition[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

float  ambientLight[4] = { 0.5f, 0.5f, 0.5f, 1.0f };
float  diffuseLight[4] = { 0.5f, 0.5f, 0.5f, 1.0f };
float specularLight[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

const float *matAmbient = BrassAmbient;
const float *matDiffuse = BrassDiffuse;
const float *matSpecular = BrassSpecular;
float matShininess = BrassShininess;

void DrawPhyramid()
{
	CVector vertices[10] = {
		{0.0f, 0.0f, 5.0f}, // G�ra fig�ry
		{0.0f, 2.0f, 0.0f}, //                1
		{1.5f, 1.5f, 0.0f}, //            9        2
		{2.0f, 0.0f, 0.0f}, //          8     0      3
	 	{1.7f, -1.2f, 0.0f}, //          7          4
		{1.0f, -2.0f, 0.0f}, //             6    5
		{-1.0f, -2.0f, 0.0f}, //
		{-1.7f, -1.2f, 0.0f}, //
		{-2.0f, 0.0f, 0.0f}, //
		{-1.5f, 1.5f, 0.0f}, //
	};

	CVector normals[10] = {
		{0.0f, -1.0f, 0.0f},
		GetNormalFromPoints(vertices[2], vertices[0], vertices[1]),
		GetNormalFromPoints(vertices[3], vertices[0], vertices[2]),
		GetNormalFromPoints(vertices[4], vertices[0], vertices[3]),
		GetNormalFromPoints(vertices[5], vertices[0], vertices[4]),
		GetNormalFromPoints(vertices[6], vertices[0], vertices[5]),
		GetNormalFromPoints(vertices[7], vertices[0], vertices[6]),
		GetNormalFromPoints(vertices[8], vertices[0], vertices[7]),
		GetNormalFromPoints(vertices[9], vertices[0], vertices[8]),
		GetNormalFromPoints(vertices[1], vertices[0], vertices[9])
	};

	glBegin(GL_TRIANGLES);
	
	glNormal3fv(normals[1].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[1].vectors);
	glVertex3fv(vertices[1].vectors);
	glNormal3fv(normals[1].vectors);
	glVertex3fv(vertices[2].vectors);

	glNormal3fv(normals[2].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[2].vectors);
	glVertex3fv(vertices[2].vectors);
	glNormal3fv(normals[3].vectors);
	glVertex3fv(vertices[3].vectors);

	glNormal3fv(normals[3].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[3].vectors);
	glVertex3fv(vertices[3].vectors);
	glNormal3fv(normals[3].vectors);
	glVertex3fv(vertices[4].vectors);

	glNormal3fv(normals[4].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[4].vectors);
	glVertex3fv(vertices[4].vectors);
	glNormal3fv(normals[4].vectors);
	glVertex3fv(vertices[5].vectors);

	glNormal3fv(normals[5].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[5].vectors);
	glVertex3fv(vertices[5].vectors);
	glNormal3fv(normals[5].vectors);
	glVertex3fv(vertices[6].vectors);

	glNormal3fv(normals[6].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[6].vectors);
	glVertex3fv(vertices[6].vectors);
	glNormal3fv(normals[6].vectors);
	glVertex3fv(vertices[7].vectors);

	glNormal3fv(normals[7].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[7].vectors);
	glVertex3fv(vertices[7].vectors);
	glNormal3fv(normals[7].vectors);
	glVertex3fv(vertices[8].vectors);

	glNormal3fv(normals[8].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[8].vectors);
	glVertex3fv(vertices[8].vectors);
	glNormal3fv(normals[8].vectors);
	glVertex3fv(vertices[9].vectors);

	glNormal3fv(normals[9].vectors);
	glVertex3fv(vertices[0].vectors);
	glNormal3fv(normals[9].vectors);
	glVertex3fv(vertices[9].vectors);
	glNormal3fv(normals[9].vectors);
	glVertex3fv(vertices[1].vectors);

	glEnd();
}

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, matShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();

	glTranslatef(0.0f, -2.0f, -10.0f);
	glRotatef(rotatex, 1.0f, 0.0, 0.0);
	glRotatef(rotatey, 0.0f, 1.0f, 0.0f);

	DrawPhyramid();

	glPopMatrix();

	glutSwapBuffers();
}

void Idle()
{
	glutPostRedisplay();
}

void Resize(int w, int h)
{
	if (h == 0)
		h = 1;

	float a = (float)w / (float)h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(75.0, a, 0.1, 100);
}

void InitScene()
{
	glClearColor(0.4, 0.7, 0.3, 1.0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	glFrontFace(GL_CW);
	glShadeModel(GL_FLAT);
}

void MouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		button_state = state;

		if (state == GLUT_DOWN)
		{
			button_x = x;
			button_y = y;
		}
	}
}

void MouseMotion(int x, int y)
{
	if (button_state == GLUT_DOWN)
	{
		rotatey -= (x - button_x)/ 2.0;
		button_x = x;
		rotatex -= (y - button_y)/ 2.0;
		button_y = y;
		glutPostRedisplay();
	}
}


void Menu(int value)
{
	switch (value)
	{
		// materia� - mosi�dz
	case BRASS:
		matAmbient = BrassAmbient;
		matDiffuse = BrassDiffuse;
		matSpecular = BrassSpecular;
		matShininess = BrassShininess;
		break;

		// materia� - br�z
	case BRONZE:
		matAmbient = BronzeAmbient;
		matDiffuse = BronzeDiffuse;
		matSpecular = BronzeSpecular;
		matShininess = BronzeShininess;
		break;

		// materia� - polerowany br�z
	case POLISHED_BRONZE:
		matAmbient = PolishedBronzeAmbient;
		matDiffuse = PolishedBronzeDiffuse;
		matSpecular = PolishedBronzeSpecular;
		matShininess = PolishedBronzeShininess;
		break;

		// materia� - chrom
	case CHROME:
		matAmbient = ChromeAmbient;
		matDiffuse = ChromeDiffuse;
		matSpecular = ChromeSpecular;
		matShininess = ChromeShininess;
		break;

		// materia� - mied�
	case COPPER:
		matAmbient = CopperAmbient;
		matDiffuse = CopperDiffuse;
		matSpecular = CopperSpecular;
		matShininess = CopperShininess;
		break;

		// materia� - polerowana mied�
	case POLISHED_COPPER:
		matAmbient = PolishedCopperAmbient;
		matDiffuse = PolishedCopperDiffuse;
		matSpecular = PolishedCopperSpecular;
		matShininess = PolishedCopperShininess;
		break;

		// materia� - z�oto
	case GOLD:
		matAmbient = GoldAmbient;
		matDiffuse = GoldDiffuse;
		matSpecular = GoldSpecular;
		matShininess = GoldShininess;
		break;

		// materia� - polerowane z�oto
	case POLISHED_GOLD:
		matAmbient = PolishedGoldAmbient;
		matDiffuse = PolishedGoldDiffuse;
		matSpecular = PolishedGoldSpecular;
		matShininess = PolishedGoldShininess;
		break;

		// materia� - grafit (cyna z o�owiem)
	case PEWTER:
		matAmbient = PewterAmbient;
		matDiffuse = PewterDiffuse;
		matSpecular = PewterSpecular;
		matShininess = PewterShininess;
		break;

		// materia� - srebro
	case SILVER:
		matAmbient = SilverAmbient;
		matDiffuse = SilverDiffuse;
		matSpecular = SilverSpecular;
		matShininess = SilverShininess;
		break;

		// materia� - polerowane srebro
	case POLISHED_SILVER:
		matAmbient = PolishedSilverAmbient;
		matDiffuse = PolishedSilverDiffuse;
		matSpecular = PolishedSilverSpecular;
		matShininess = PolishedSilverShininess;
		break;

		// materia� - szmaragd
	case EMERALD:
		matAmbient = EmeraldAmbient;
		matDiffuse = EmeraldDiffuse;
		matSpecular = EmeraldSpecular;
		matShininess = EmeraldShininess;
		break;

		// materia� - jadeit
	case JADE:
		matAmbient = JadeAmbient;
		matDiffuse = JadeDiffuse;
		matSpecular = JadeSpecular;
		matShininess = JadeShininess;
		break;

		// materia� - obsydian
	case OBSIDIAN:
		matAmbient = ObsidianAmbient;
		matDiffuse = ObsidianDiffuse;
		matSpecular = ObsidianSpecular;
		matShininess = ObsidianShininess;
		break;

		// materia� - per�a
	case PEARL:
		matAmbient = PearlAmbient;
		matDiffuse = PearlDiffuse;
		matSpecular = PearlSpecular;
		matShininess = PearlShininess;
		break;

		// metaria� - rubin
	case RUBY:
		matAmbient = RubyAmbient;
		matDiffuse = RubyDiffuse;
		matSpecular = RubySpecular;
		matShininess = RubyShininess;
		break;

		// materia� - turkus
	case TURQUOISE:
		matAmbient = TurquoiseAmbient;
		matDiffuse = TurquoiseDiffuse;
		matSpecular = TurquoiseSpecular;
		matShininess = TurquoiseShininess;
		break;

		// materia� - czarny plastik
	case BLACK_PLASTIC:
		matAmbient = BlackPlasticAmbient;
		matDiffuse = BlackPlasticDiffuse;
		matSpecular = BlackPlasticSpecular;
		matShininess = BlackPlasticShininess;
		break;

		// materia� - czarna guma
	case BLACK_RUBBER:
		matAmbient = BlackRubberAmbient;
		matDiffuse = BlackRubberDiffuse;
		matSpecular = BlackRubberSpecular;
		matShininess = BlackRubberShininess;
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Program 5");

	InitScene();

	glutDisplayFunc(Display);
	glutIdleFunc(Idle);
	glutReshapeFunc(Resize);
	glutMotionFunc(MouseMotion);
	glutMouseFunc(MouseButton);

	glutCreateMenu(Menu);
	glutAddMenuEntry("Mosi�dz", BRASS);
	glutAddMenuEntry("Br�z", BRONZE);
	glutAddMenuEntry("Polerowany br�z", POLISHED_BRONZE);
	glutAddMenuEntry("Chrom", CHROME);
	glutAddMenuEntry("Mied�", COPPER);
	glutAddMenuEntry("Polerowana mied�", POLISHED_COPPER);
	glutAddMenuEntry("Z�oto", GOLD);
	glutAddMenuEntry("Polerowane z�oto", POLISHED_GOLD);
	glutAddMenuEntry("Grafit (cyna z o�owiem)", PEWTER);
	glutAddMenuEntry("Srebro", SILVER);
	glutAddMenuEntry("Polerowane srebro", POLISHED_SILVER);
	glutAddMenuEntry("Szmaragd", EMERALD);
	glutAddMenuEntry("Jadeit", JADE);
	glutAddMenuEntry("Obsydian", OBSIDIAN);
	glutAddMenuEntry("Per�a", PEARL);
	glutAddMenuEntry("Rubin", RUBY);
	glutAddMenuEntry("Turkus", TURQUOISE);
	glutAddMenuEntry("Czarny plastik", BLACK_PLASTIC);
	glutAddMenuEntry("Czarna guma", BLACK_RUBBER);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	glutMainLoop();

	return 0;
}